<?php
// connexion à la base de donnees
// lecture des messages enregistrees
require "bdd/bddconfig.php";

$pseudok = isset($_POST["pseudo"]);
$message = isset($_POST["commentaire"]);

// securisation des variables
if (($pseudok) && ($message)) {
    $pseudok = strval(htmlspecialchars($_POST["pseudo"]));
    $message = strval(htmlspecialchars($_POST["commentaire"]));

    // insert dans la base
    // requete sql

    try {
        $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;
    charset=utf8", $bddlogin, $bddpass);
        $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $RSLOGIN = $objBdd->prepare("insert INTO messages (pseudo,commentaire) VALUES (:pseudo, :commentaire)");
        $RSLOGIN->bindParam(':pseudo', $pseudok, PDO::PARAM_STR);
        $RSLOGIN->bindParam(':commentaire', $message, PDO::PARAM_STR);
        $RSLOGIN->execute();

        // recupérer la valeur de l'id du nouveau bassin crée
        $lastId = $objBdd->lastInsertId();
    } catch (Exception $prmE) {
        die('Erreur ; ' . $prmE->getMessage());
    }

    // rediriger uniquement vers la page bassin.php
    // header ("Location:http://localhost/truites/bassins.php");
    // remplace par :
    $serveur = $_SERVER['HTTP_HOST'];
    $chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $page = 'index.php';
    header("Location: http://$serveur$chemin/$page");
} else {


    //// fin ajout



    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);

    try {
        $objBdd = new PDO(
            "mysql:host=$bddserver;
        dbname=$bddname;
        charset=utf8",
            $bddlogin,
            $bddpass
        );

        $objBdd->setAttribute(
            PDO::ATTR_ERRMODE,
            PDO::ERRMODE_EXCEPTION
        );
        $listemessages = $objBdd->query("SELECT * FROM messages");
    } catch (Exception $prmE) {
        die('Erreur : ' . $prmE->getMessage());
    }
?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8" />
        <title><?php $titre; ?></title>
        <link rel="stylesheet" href="css/styles.css" />
    </head>

    <body>
        <article>
            <h1>Livre d'or</h1>
            <table>
                <thead>
                    <tr>
                        <th colspan="2">Les messages</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    while ($unmessage = $listemessages->fetch()) {
                    ?>
                        <tr>
                            <td>Le : <?= $unmessage['date']; ?></td>
                            <td><?= $unmessage['commentaire']; ?> (<?= $unmessage['pseudo']; ?>)</td>
                        </tr>
                    <?php
                        } //fin du while

                    } ?>
                </tbody>
            </table>
        <?php
    $listemessages->closeCursor(); //libère les ressources de la bdd 
        ?>
        <p></p>
        <form method="POST" action="index.php">
            <fieldset>
                <legend>Ajouter votre commentaire au livre d'or</legend>
                Pseudo :<br />
                <input type="text" name="pseudo" value="" placeholder="votre pseudo" required>
                <br />
                Message :<br>
                <textarea name="commentaire" rows="10" cols="40" placeholder="Votre commentaire" required></textarea>
                <br />
                <input type="submit" value="Enregistrer">
            </fieldset>
        </form>
        </article>
    </body>
    </html>